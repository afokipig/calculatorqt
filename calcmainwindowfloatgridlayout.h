#ifndef CALCMAINWINDOWFLOATGRIDLAYOUT_H
#define CALCMAINWINDOWFLOATGRIDLAYOUT_H

#include <QObject>
#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include "Types.h"

class CalcMainWindowFloatGridLayout : public QGridLayout
{
    Q_OBJECT

public:
    CalcMainWindowFloatGridLayout(QWidget *parent);
    ~CalcMainWindowFloatGridLayout();

signals:
    void ButtonPressed(int Value);
    void SignifireButtonPressed(Signifires Sign);

public slots:
    void ReciveAllValues(float FirsValue, float SeconValue, Signifires Sign, float Result, bool bResultCondition);

private slots:
    void ButtonZeroPressed();
    void ButtonOnePressed();
    void ButtonTwoPressed();
    void ButtonThreePressed();
    void ButtonFourPressed();
    void ButtonFivePressed();
    void ButtonSixPressed();
    void ButtonSevenPressed();
    void ButtonEightPressed();
    void ButtonNinePressed();
    void ButtonDeletePressed();
    void ButtonEqualPressed();
    void ButtonPlusPressed();
    void ButtonMinusPressed();
    void ButtonMyltiplyPressed();
    void ButtonDivisionPressed();

private:
    QRect GetPosition(int X, int Y, int Size = 40);
    QLabel *Values;
};

#endif // CALCMAINWINDOWFLOATGRIDLAYOUT_H

#include "calculatingcore.h"
#include "Types.h"
#include <iostream>
#include <fstream>

CalculatingCore::CalculatingCore(QObject *parent) : QObject(parent)
{
    bisFirstValue = true;
    std::ifstream MemoryLog;
    MemoryLog.open("..\\log.txt", std::ifstream::in);
    if(MemoryLog.is_open())
    {
        MemoryLog >> FirstValue >> SecondValue >> Result >> bResultCondition;
    }
    MemoryLog.close();
}

CalculatingCore::CalculatingCore(std::string Message, QObject *parent) : QObject(parent), MyMessage(Message)
{
    bisFirstValue = true;
    std::cout << MyMessage <<'\n';
}

std::string& CalculatingCore::operator+(const std::string &NewMessage)
{
    MyMessage.append(NewMessage);
    return MyMessage;
}

void CalculatingCore::ReciveValues(int Value)
{
    std::cout << Value << '\n';
    if(bisFirstValue)
    {
        FirstRawValue.push_back(Value);
    }
    else
    {
        SecondRawValue.push_back(Value);
    }
    UpdateValues();
}

void CalculatingCore::ResiveSignifire(Signifires Sign)
{
    switch (Sign)
    {
    case Signifires::DOT:
        if(bisFirstValue)
        {
            if(bFirstValueIsInteger)
                FirstRawValue.push_back(-1);
        }
        else
        {
            if(bSecondValueIsInteger)
                SecondRawValue.push_back(-1);
        }
        break;
    case Signifires::NONE:
        break;
    case Signifires::DELETE:
        if(!bisFirstValue)
        {
            if(!SecondRawValue.empty())
            {
                SecondRawValue.pop_back();
            }
            else
            {
                bisFirstValue = true;
            }
        }
        else
        {
            if(!FirstRawValue.empty())
                FirstRawValue.pop_back();
        }
        break;
    case Signifires::EQUAL:
        CalculateResult();
        break;
    default:
        CurrentSign = Sign;
        bisFirstValue = false;
        break;
    }
    UpdateValues();
}

void CalculatingCore::UpdateValues()
{
    FirstValue = 0;
    SecondValue = 0;
    int FirstValueDotPosition = 0;
    int FirstValueSize = static_cast<int>(FirstRawValue.size());
    for(int i = 0; i < FirstValueSize; i++)
    {
        if(FirstRawValue.at(i) == -1)
        {
            FirstValueDotPosition = i;
            continue;
        }
        FirstValue = FirstValue * 10 + FirstRawValue.at(i);
    }

    if (FirstValueDotPosition != 0)
    {
        FirstValue = FirstValue /(10 * FirstValueDotPosition);
        bFirstValueIsInteger = false;
    }
    int SecondValueDotPosition = 0;
    int SecondValueSize = static_cast<int>(SecondRawValue.size());
    for(int i = 0; i < SecondValueSize; i++)
    {
        if(SecondRawValue.at(i) == -1)
        {
            SecondValueDotPosition = i;
            continue;
        }
        SecondValue = SecondValue * 10 + SecondRawValue.at(i);
    }

    if(SecondValueDotPosition != 0)
    {
        SecondValue = SecondValue / (10 * SecondValueDotPosition);
        bSecondValueIsInteger = false;
    }
    emit SendAllValues(FirstValue, SecondValue, CurrentSign, Result, bResultCondition);
    if(bResultCondition)
    {
        FirstRawValue.clear();
        SecondRawValue.clear();
        CurrentSign = Signifires::NONE;
        bisFirstValue = true;
        bResultCondition = false;
    }
    std::ofstream MemoryLog;
    MemoryLog.open("..\\log.txt", std::ofstream::out | std::ofstream::trunc);
    if(MemoryLog.is_open())
    {
        MemoryLog << FirstValue << ' ' << SecondValue << ' ' << Result << ' ' << bResultCondition;
        std::cout <<"ToStr" << FirstValue << SecondValue << Result << bResultCondition << '\n';
    }
    MemoryLog.close();
}

void CalculatingCore::CalculateResult()
{
    bResultCondition = true;
    switch (CurrentSign)
    {
    case Signifires::PLUS:
        Result = FirstValue + SecondValue;
        break;
    case Signifires::MINUS:
        Result = FirstValue - SecondValue;
        break;
    case Signifires::MULTIPLICATION:
        Result = FirstValue * SecondValue;
        break;
    case Signifires::DIVISION:
        Result = FirstValue / SecondValue;
        break;
    default:
        Result = 0;
        bResultCondition = false;
        break;
    }
}

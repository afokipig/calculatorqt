#include "calculatorwindows.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CalculatorWindows w;
    w.show();
    return a.exec();
}

#ifndef CALCULATORWINDOWS_H
#define CALCULATORWINDOWS_H

#include <QObject>
#include <QMainWindow>
#include <QGridLayout>
#include "calcmainwindowfloatgridlayout.h"
#include "Types.h"
#include "calculatingcore.h"

class CalculatorWindows : public QMainWindow
{
    Q_OBJECT

public:
    CalculatorWindows(QWidget *parent = nullptr);
    ~CalculatorWindows();

private:
    QRect GetPosition(int X, int Y, int Size = 40);
    CalcMainWindowFloatGridLayout *Grid;
    CalculatingCore* CalcCore;
};
#endif // CALCULATORWINDOWS_H

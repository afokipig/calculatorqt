#ifndef TYPES_H
#define TYPES_H

enum class Signifires
{
    NONE,
    DELETE,
    EQUAL,
    PLUS,
    MINUS,
    MULTIPLICATION,
    DIVISION,
    DOT
};

#endif // TYPES_H

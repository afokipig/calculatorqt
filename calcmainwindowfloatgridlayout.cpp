#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include "calcmainwindowfloatgridlayout.h"

CalcMainWindowFloatGridLayout::CalcMainWindowFloatGridLayout(QWidget *parent)
    :QGridLayout(parent)
{
    QPushButton *ButtonOne = new QPushButton("1", parent);
    ButtonOne->setGeometry(GetPosition(0, 6));
    connect(ButtonOne, &QPushButton::clicked, this, &CalcMainWindowFloatGridLayout::ButtonOnePressed);

    QPushButton *ButtonTwo = new QPushButton("2", parent);
    ButtonTwo->setGeometry(GetPosition(1, 6));
    connect(ButtonTwo, &QPushButton::clicked, this, &CalcMainWindowFloatGridLayout::ButtonTwoPressed);

    QPushButton *ButtonThree = new QPushButton("3", parent);
    ButtonThree->setGeometry(GetPosition(2, 6));
    connect(ButtonThree, &QPushButton::clicked, this, &CalcMainWindowFloatGridLayout::ButtonThreePressed);

    QPushButton *ButtonFour = new QPushButton("4", parent);
    ButtonFour->setGeometry(GetPosition(0, 5));
    connect(ButtonFour, &QPushButton::clicked, this, &CalcMainWindowFloatGridLayout::ButtonFourPressed);

    QPushButton *ButtonFive = new QPushButton("5", parent);
    ButtonFive->setGeometry(GetPosition(1, 5));
    connect(ButtonFive, &QPushButton::clicked, this, &CalcMainWindowFloatGridLayout::ButtonFivePressed);

    QPushButton *ButtonSix = new QPushButton("6", parent);
    ButtonSix->setGeometry(GetPosition(2, 5));
    connect(ButtonSix, &QPushButton::clicked, this, &CalcMainWindowFloatGridLayout::ButtonSixPressed);

    QPushButton *ButtonSeven = new QPushButton("7", parent);
    ButtonSeven->setGeometry(GetPosition(0, 4));
    connect(ButtonSeven, &QPushButton::clicked, this, &CalcMainWindowFloatGridLayout::ButtonSevenPressed);

    QPushButton *ButtonEight = new QPushButton("8", parent);
    ButtonEight->setGeometry(GetPosition(1, 4));
    connect(ButtonEight, &QPushButton::clicked, this, &CalcMainWindowFloatGridLayout::ButtonEightPressed);

    QPushButton *ButtonNine = new QPushButton("9", parent);
    ButtonNine->setGeometry(GetPosition(2, 4));
    connect(ButtonNine, &QPushButton::clicked, this, &CalcMainWindowFloatGridLayout::ButtonNinePressed);

    QPushButton *ButtonZero = new QPushButton("0", parent);
    ButtonZero->setGeometry(GetPosition(0, 7));
    connect(ButtonZero, &QPushButton::clicked, this, &CalcMainWindowFloatGridLayout::ButtonZeroPressed);

    QPushButton *ButtonDelete = new QPushButton("Del", parent);
    ButtonDelete->setGeometry(GetPosition(1, 7));
    connect(ButtonDelete, &QPushButton::clicked, this, &CalcMainWindowFloatGridLayout::ButtonDeletePressed);

    QPushButton *ButtonEqual = new QPushButton("=", parent);
    ButtonEqual->setGeometry(GetPosition(2, 7));
    connect(ButtonEqual, &QPushButton::clicked, this, &CalcMainWindowFloatGridLayout::ButtonEqualPressed);

    QPushButton *ButtonPlus = new QPushButton("+", parent);
    ButtonPlus->setGeometry(GetPosition(3, 7));
    connect(ButtonPlus, &QPushButton::clicked, this, &CalcMainWindowFloatGridLayout::ButtonPlusPressed);

    QPushButton *ButtonMinus = new QPushButton("-", parent);
    ButtonMinus->setGeometry(GetPosition(3, 6));
    connect(ButtonMinus, &QPushButton::clicked, this, &CalcMainWindowFloatGridLayout::ButtonMinusPressed);

    QPushButton *ButtonMyltiply = new QPushButton("*", parent);
    ButtonMyltiply->setGeometry(GetPosition(3, 5));
    connect(ButtonMyltiply, &QPushButton::clicked, this, &CalcMainWindowFloatGridLayout::ButtonMyltiplyPressed);

    QPushButton *ButtonDivision = new QPushButton("/", parent);
    ButtonDivision->setGeometry(GetPosition(3, 4));
    connect(ButtonDivision, &QPushButton::clicked, this, &CalcMainWindowFloatGridLayout::ButtonDivisionPressed);

    QLabel *Description = new QLabel("First value\nSignifire\nSecond value\nResult", parent);
    Description->setGeometry(GetPosition(0, 0, 80));

    Values = new QLabel(parent);
    Values->setGeometry(GetPosition(2, 0, 80));
    Values->setText("Test");
}

CalcMainWindowFloatGridLayout::~CalcMainWindowFloatGridLayout()
{

}

void CalcMainWindowFloatGridLayout::ReciveAllValues(float FirsValue, float SeconValue, Signifires Sign, float Result, bool bResultCondition)
{
    QString Text;
    Text.push_back(QString::number(FirsValue));
    Text.push_back('\n');
    switch (Sign)
    {
    case Signifires::PLUS:
        Text.push_back("+\n");
        break;
    case Signifires::MINUS:
        Text.push_back("-\n");
        break;
    case Signifires::MULTIPLICATION:
        Text.push_back("*\n");
        break;
    case Signifires::DIVISION:
        Text.push_back("/\n");
        break;
    default:
        Text.push_back('\n');
        break;

    }
    Text.push_back(QString::number(SeconValue));
    Text.push_back('\n');
    if(bResultCondition)
    {
        Text.push_back(QString::number(Result));
    }

    Values->setText(Text);
}


void CalcMainWindowFloatGridLayout::ButtonZeroPressed()
{
    emit ButtonPressed(0);
}

void CalcMainWindowFloatGridLayout::ButtonOnePressed()
{
    emit ButtonPressed(1);
}

void CalcMainWindowFloatGridLayout::ButtonTwoPressed()
{
    emit ButtonPressed(2);
}

void CalcMainWindowFloatGridLayout::ButtonThreePressed()
{
    emit ButtonPressed(3);
}

void CalcMainWindowFloatGridLayout::ButtonFourPressed()
{
    emit ButtonPressed(4);
}

void CalcMainWindowFloatGridLayout::ButtonFivePressed()
{
    emit ButtonPressed(5);
}

void CalcMainWindowFloatGridLayout::ButtonSixPressed()
{
    emit ButtonPressed(6);
}

void CalcMainWindowFloatGridLayout::ButtonSevenPressed()
{
    emit ButtonPressed(7);
}

void CalcMainWindowFloatGridLayout::ButtonEightPressed()
{
    emit ButtonPressed(8);
}

void CalcMainWindowFloatGridLayout::ButtonNinePressed()
{
    emit ButtonPressed(9);
}

void CalcMainWindowFloatGridLayout::ButtonDeletePressed()
{
    emit SignifireButtonPressed(Signifires::DELETE);
}

void CalcMainWindowFloatGridLayout::ButtonEqualPressed()
{
    emit SignifireButtonPressed(Signifires::EQUAL);
}

void CalcMainWindowFloatGridLayout::ButtonPlusPressed()
{
    emit SignifireButtonPressed(Signifires::PLUS);
}

void CalcMainWindowFloatGridLayout::ButtonMinusPressed()
{
    emit SignifireButtonPressed(Signifires::MINUS);
}

void CalcMainWindowFloatGridLayout::ButtonMyltiplyPressed()
{
    emit SignifireButtonPressed(Signifires::MULTIPLICATION);
}

void CalcMainWindowFloatGridLayout::ButtonDivisionPressed()
{
    emit SignifireButtonPressed(Signifires::DIVISION);
}


QRect CalcMainWindowFloatGridLayout::GetPosition(int X, int Y, int Size)
{
    QRect Position = {Size * X, Size * Y, Size, Size};
    return Position;
}

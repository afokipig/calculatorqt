#ifndef CALCULATINGCORE_H
#define CALCULATINGCORE_H

#include <QObject>
#include <QWidget>
#include "Types.h"
#include <vector>

class CalculatingCore : public QObject
{
    Q_OBJECT
public:
    explicit CalculatingCore(QObject *parent = nullptr);
    explicit CalculatingCore(std::string Message, QObject *parent = nullptr);

    std::string MyMessage;
    std::string& operator+(const std::string &NewMessage);

signals:
    void SendAllValues(float FirsValue, float SeconValue, Signifires Sign, float Result, bool bResultCondition);

public slots:
    void ReciveValues(int Value);
    void ResiveSignifire(Signifires Sign);

private:
    bool bisFirstValue = true;
    bool bFirstValueIsInteger;
    bool bSecondValueIsInteger;
    std::vector<int> FirstRawValue;
    float FirstValue;
    std::vector<int> SecondRawValue;
    float SecondValue;
    Signifires CurrentSign;
    float Result;
    bool bResultCondition;

    void UpdateValues();
    void DeleteSymbol();
    void ChangeSignifire(Signifires Sign);
    void CalculateResult();
};

#endif // CALCULATINGCORE_H

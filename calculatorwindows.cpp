#include "calculatorwindows.h"
#include "calcmainwindowfloatgridlayout.h"
#include <QGridLayout>
#include <QPushButton>
#include "Types.h"

#include <iostream>

CalculatorWindows::CalculatorWindows(QWidget *parent)
    : QMainWindow(parent)
{
    CalcCore = new CalculatingCore("ThisIsTestFunctional",this);
    *CalcCore + "\tYep, Test\n";
    std::cout << CalcCore->MyMessage << '\n';

    Grid = new CalcMainWindowFloatGridLayout(this);
    Grid->setSpacing(10);

    resize(500, 500);

    setLayout(Grid);

    connect(Grid, &CalcMainWindowFloatGridLayout::SignifireButtonPressed, CalcCore, &CalculatingCore::ResiveSignifire);
    connect(Grid, &CalcMainWindowFloatGridLayout::ButtonPressed, CalcCore, &CalculatingCore::ReciveValues);
    connect(CalcCore, &CalculatingCore::SendAllValues, Grid, &CalcMainWindowFloatGridLayout::ReciveAllValues);
}

CalculatorWindows::~CalculatorWindows()
{
    delete CalcCore;
    delete Grid;
}

QRect CalculatorWindows::GetPosition(int X, int Y, int Size)
{
    QRect Position = {Size * X, Size * Y, Size, Size};
    return Position;
}


